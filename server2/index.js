const express= require('express');
const cors=require('cors');
const bodyParser=require('body-parser');
var mongoose= require('mongoose');
const Input= require('./Models/input.js');
const User= require('./Models/User.js');
const bcrypt= require('bcrypt');
const emailValidator= require('email-validator');

const app= express();

var Url="mongodb://localhost:27017/nodeDb";

//connet to database
mongoose.connect(Url);

var db=mongoose.connection;

db.on("error",()=>{
    console.log("error connectine");
});

db.once("open", ()=>{
    console.log("connected");
})

// for cors
app.use(cors());

//for body parsing
app.use(bodyParser.json());

app.post('/userInput',async (req,res)=>{
    var input= new Input({
        userInput: req.body.input
    });

    try{
        await input.save();

    }
    catch(e)
    {
        console.log("exception "+ e);
    }

});

app.post("/registerUser",async (req,res)=>{
    const existUser= await User.findOne({
        email: req.body.email
    });
    var password= req.body.password;
    var email= req.body.email;

    // var user=new User({
    //     email: req.body.email,
    //     password: req.body.password
    // });

    console.log("exsisting user");
    console.log(existUser +"hio");
    if(existUser)
    {
        res.status(400).json({message: "user already exsists"});
    }
    else if( !(await emailValidator.validate(email)))
    {
        res.status(400).json({message: "Invalid Email"});
    }
    else if(password.length <8){
        res.status(500).json({message: "password must be greater or equal to 8"});
    }
    else{
        console.log("under else");
        console.log(password);
        var salt=await bcrypt.genSalt(10);
        var hashPassword= await bcrypt.hash(password.toString(),salt);
        console.log("hashPasswor");
        console.log(hashPassword);
        
         var user=new User({
        email: email,
        password: hashPassword
    });
        try{
            await user.save();
            res.json({"user": user,message : "successful"});

        }
        catch(e)
        {
            console.log(e);
            res.json({"user": null,message : "error"});

        }
       

    }

});

//get method

app.get("/",(req,res)=>{
    res.send("Hello World!");

});


//for listening
app.listen(8000,()=>{
    console.log("App started");

})