
const mongoose= require('mongoose');

var UserSch= mongoose.Schema({
    email:{
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    }
});

var User= mongoose.model('User',UserSch);

module.exports= User