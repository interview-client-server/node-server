import React,{ Component} from 'react';
import {Button,Form} from 'react-bootstrap';


export default class Login extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={
            email: "",
            password:""
        };

        this.handleEmail=this.handleEmail.bind(this);
        this.handlePassword=this.handlePassword.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);

    }

    handleEmail(event)
    {
        this.setState({
            email: event.target.value
        });
    }
    handlePassword(event)
    {
        this.setState({
            password: event.target.value
        });
    }
    handleSubmit(event)
    {
        var requestOptions={
            method : "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password
            })
        }

        fetch("http://localhost:8000/registerUser",requestOptions).then(async (res)=>{
            let response=await res.json();
            alert(response.message);
        }).catch((error)=>{
            alert("error is"+error);
        });

        event.preventDefault();
    }

    render()
    {
        return(
            <div className="row">
                <Form onSubmit={this.handleSubmit} className="col-md-6 ml-3" style={{"margin-left":"30px"}}>
                    <Form.Label>Email </Form.Label>
                    <Form.Control type="email" onChange={this.handleEmail}/>
                    <br/>
                    <br/>
                    <Form.Label>Password </Form.Label>
                    <Form.Control type="password" onChange={this.handlePassword}/>
                    <br/>
                    <br/>
                    <Button type="submit" value="submit" >
                       Save
                    </Button>
                </Form>
            </div>
        );
    }
}