import React, { Component } from 'react';
import { Container, Row } from 'react-bootstrap';
import { Button, Col, Form } from 'react-bootstrap';


export default class UserInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: ""
        };

        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleInput(event) {
        this.setState({
            input: event.target.value
        });
    }
    handleSubmit(event) {
        var requestOptions = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                input: this.state.input
            })
        }

        fetch("http://localhost:8000/userInput", requestOptions).then((res) => {
            // res.json("saved");
            alert("saved");
        }).catch((error) => {
            alert(error);
        })

        event.preventDefault();
    }

    render() {
        return (
            <div className="row">
                <br />
                <Container className="ml-3">
                    <Row>
                        <Col>
                            <Form onSubmit={this.handleSubmit} className="col-md-6 ml-3" style={{"margin-left":"30px"}}>
                                <br />
                                <br />
                                <Form.Control type="text" placeholder="input" onChange={this.handleInput} />
                                <br />
                                <br />
                                <Button type="submit" value="submit">
                                    Save
                                </Button>
                            </Form>
                        </Col>

                    </Row>
                </Container>

            </div>
        );
    }
}